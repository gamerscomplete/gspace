package main

import (
	"encoding/json"
	"fmt"
	"io/ioutil"
	"net/http"
)

type Person struct {
	Name  string `json:"name"`
	Craft string `json:"craft"`
}

type Astros struct {
	Message string   `json:"message"`
	Number  int      `json:"number"`
	People  []Person `json:"people"`
}

func main() {
	url := "http://api.open-notify.org/astros.json"

	resp, err := http.Get(url)
	if err != nil {
		fmt.Printf("Error: %s", err)
		return
	}
	defer resp.Body.Close()

	body, err := ioutil.ReadAll(resp.Body)
	if err != nil {
		fmt.Printf("Error: %s", err)
		return
	}

	var astros Astros
	err = json.Unmarshal(body, &astros)
	if err != nil {
		fmt.Printf("Error: %s", err)
		return
	}

	fmt.Printf("There are %d people in space:\n", astros.Number)
	for _, p := range astros.People {
		fmt.Printf("%s is aboard %s\n", p.Name, p.Craft)
	}
}
