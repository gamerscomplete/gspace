# Go Astros Program

This Go program sends a GET request to the "http://api.open-notify.org/astros.json" URL, which provides information about the astronauts currently in space. The program reads and parses the JSON response, and prints the number of people currently in space, along with each person's name and the craft they are on.

## Installation

1. Make sure you have [Go installed](https://golang.org/doc/install) on your machine.

2. Clone this repository or copy the Go code to a file, e.g., `astros.go`.

## Usage

1. Open a terminal.

2. Navigate to the directory where you have the `astros.go` file.

3. Run the program with the command: `go run astros.go`

The output will look similar to this:

```plaintext
There are 10 people in space:
John Doe is aboard ISS
Jane Doe is aboard ISS
...
